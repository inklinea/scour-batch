#!/usr/bin/env python
"""
Run the scour module on the svg output.
"""


import inkex
from inkex.localization import inkex_gettext as _

# Inklinea, added batch support
from save_batch import *

try:
    from packaging.version import Version
except ImportError:
    raise inkex.DependencyError(
        _(
            """Failed to import module 'packaging'.
Please make sure it is installed (e.g. using 'pip install packaging'
or 'sudo apt-get install python3-packaging') and try again.
"""
        )
    )

try:
    import scour
    from scour.scour import scourString
except ImportError:
    raise inkex.DependencyError(
        _(
            """Failed to import module 'scour'.
Please make sure it is installed (e.g. using 'pip install scour'
  or 'sudo apt-get install python3-scour') and try again.
"""
        )
    )


class ScourBatch(inkex.EffectExtension):
    """Scour Inkscape Extension"""

    # Scour options
    def add_arguments(self, pars):
        pars.add_argument("--tab")
        pars.add_argument("--simplify-colors", type=inkex.Boolean, dest="simple_colors")
        pars.add_argument("--style-to-xml", type=inkex.Boolean)
        pars.add_argument(
            "--group-collapsing", type=inkex.Boolean, dest="group_collapse"
        )
        pars.add_argument("--create-groups", type=inkex.Boolean, dest="group_create")
        pars.add_argument("--enable-id-stripping", type=inkex.Boolean, dest="strip_ids")
        pars.add_argument("--shorten-ids", type=inkex.Boolean)
        pars.add_argument("--shorten-ids-prefix")
        pars.add_argument("--embed-rasters", type=inkex.Boolean)
        pars.add_argument(
            "--keep-unreferenced-defs", type=inkex.Boolean, dest="keep_defs"
        )
        pars.add_argument("--keep-editor-data", type=inkex.Boolean)
        pars.add_argument("--remove-metadata", type=inkex.Boolean)
        pars.add_argument("--strip-xml-prolog", type=inkex.Boolean)
        pars.add_argument("--set-precision", type=int, dest="digits")
        pars.add_argument("--indent", dest="indent_type")
        pars.add_argument("--nindent", type=int, dest="indent_depth")
        pars.add_argument("--line-breaks", type=inkex.Boolean, dest="newlines")
        pars.add_argument(
            "--strip-xml-space", type=inkex.Boolean, dest="strip_xml_space_attribute"
        )
        pars.add_argument("--protect-ids-noninkscape", type=inkex.Boolean)
        pars.add_argument("--protect-ids-list")
        pars.add_argument("--protect-ids-prefix")
        pars.add_argument("--enable-viewboxing", type=inkex.Boolean)
        pars.add_argument(
            "--enable-comment-stripping", type=inkex.Boolean, dest="strip_comments"
        )
        pars.add_argument("--renderer-workaround", type=inkex.Boolean)

        # options for internal use of the extension
        pars.add_argument("--scour-version")
        pars.add_argument("--scour-version-warn-old", type=inkex.Boolean)

        # Inklinea added Batch export
        # Input Folder
        pars.add_argument("--batch_input_folder", type=str, dest="batch_input_folder", default=None)
        # Output Folder
        pars.add_argument("--batch_output_folder", type=str, dest="batch_output_folder", default=None)

    def effect(self):
        # version check if enabled in options
        if self.options.scour_version_warn_old:
            scour_version = scour.__version__
            scour_version_min = self.options.scour_version
            if Version(scour_version) < Version(scour_version_min):
                raise inkex.AbortExtension(
                    f"""
The extension 'Optimized SVG Output' is designed for Scour {scour_version_min} or later but you're
 using the older version Scour {scour_version}.

Note: You can permanently disable this message on the 'About' tab of the extension window."""
                )
        del self.options.scour_version
        del self.options.scour_version_warn_old

        input_folder = self.options.batch_input_folder

        input_file_list = get_files_in_folder(self, input_folder, extension='svg')

        output_folder = self.options.batch_output_folder

        check_output_filepath(self, output_folder, 'test.svg')

        # Exit if no input files found
        if len(input_file_list) < 1:
            inkex.errormsg('No input files found')
            sys.exit()

        for input_file in input_file_list:
            # Try block - just to prevent batch run from ending in event of error
            try:
                if os.path.isdir(input_file):
                    continue
                else:
                    loaded_svg = inkex.load_svg(input_file)
                    svg_element = loaded_svg.getroot()

                    # do the scouring
                    scour_svg_string = scourString(svg_element.tostring(), self.options)

                    # Lets write to folder

                    folder, filename = os.path.split(input_file)

                    new_filename = f'{filename.split(".svg")[0]}_scour.svg'

                    if not check_filepath_writable(self, output_folder, new_filename):
                        inkex.errormsg('cannot write to output folder')
                        continue
                    new_svg_filepath = os.path.join(output_folder, new_filename)
                    with open(new_svg_filepath, 'w') as new_svg_file:
                        new_svg_file.write(scour_svg_string)
            except:
                pass

        sys.exit()

if __name__ == "__main__":
    ScourBatch().run()
