Scour Batch - an Inkscape 1.2+ extension

Appears under Extensions>Export> Batch Optimized SVG Export

This is a modification of the File>Save as>Optimised SVG output extension

which is part of the Inkscape core extension set.

It is a basic conversion from an output extension into an effect extension.

Takes and Input Folder containing SVG files, runs Scour optimisation then saves the result in the export folder.
