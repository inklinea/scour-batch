#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Save Batch - process .svg files in a folder
# An Inkscape 1.2+ extension
##############################################################################

import inkex

import sys, os
import uuid


def check_output_filepath(self, export_folder, base_filename):
    import os
    # Test Export Folder writable with known good filename
    test_filename = str(uuid.uuid1())
    if check_filepath_writable(self, export_folder, test_filename):
        None
    else:
        inkex.errormsg('Export Folder is *Not Writeable*')
        sys.exit()
    # Test Export Filename
    if check_filepath_writable(self, export_folder, base_filename):
        # inkex.errormsg('Export Folder is okay')
        export_filepath = os.path.join(export_folder, base_filename)
        return export_filepath
    else:
        inkex.errormsg('Base Filename is *invalid*')
        sys.exit()


def check_filepath_writable(self, folderpath, filename):
    import os

    filepath = os.path.join(folderpath, filename)

    try:
        try_file = open(filepath, 'w')
        try_file.close()
        os.remove(filepath)
        return True
    except:
        return False


def get_files_in_folder(self, folder, extension=None):
    filepath_list = []
    try:
        files = os.listdir(folder)
    except:
        inkex.errormsg('Input folder invalid')
        sys.exit()

    if len(files) < 1:
        inkex.errormsg('No Files Found')
        return None

    for file in files:
        if file.endswith(extension):
            filepath_list.append(os.path.join(folder, file))

    return filepath_list
